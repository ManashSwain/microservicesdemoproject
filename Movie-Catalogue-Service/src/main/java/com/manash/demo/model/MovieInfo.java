package com.manash.demo.model;

import lombok.Data;

@Data
public class MovieInfo {
	
	private String movieId;
	private String movieName;
	private String desc;
	

}
