package com.manash.demo.model;

import lombok.Data;

@Data
public class MovieRating {
	
	private String movieId;
	private int rating;
	
	

}
