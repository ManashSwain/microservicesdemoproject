package com.manash.demo.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class UserMovieDetails {
	
	private String userId;
	private List<MovieRating> movieRatingList;
	private List<MovieInfo> movieInfoList;

}
