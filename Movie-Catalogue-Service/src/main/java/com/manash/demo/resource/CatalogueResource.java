package com.manash.demo.resource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.manash.demo.feignclient.MovieServiceFeignCleint;
import com.manash.demo.model.MovieInfo;
import com.manash.demo.model.MovieRating;
import com.manash.demo.model.UserMovieDetails;

@RestController
@RequestMapping("/movies")
public class CatalogueResource {

	private static final String ratingUrl = "http://movie-rating-service/rating/";
	private static final String infoUrl = "http://movie-info-service/info/";
	
	@Autowired
	private RestTemplate template;
	
	@Autowired
	private MovieServiceFeignCleint movieServiceFeignClient;
	
	

	@RequestMapping(value = "/{userId}", produces = "application/json")
	public UserMovieDetails getMovieList(@PathVariable("userId") String userId) {
		String movieId = "101";
		MovieRating rating = null;
		MovieInfo info = null;
		UserMovieDetails movieDetails = null;
		List<MovieRating> movieRatingList = null;
		List<MovieInfo> movieInfoList = null;
		// get rating info from MovieRating Service
		rating = template.getForObject(ratingUrl + movieId, MovieRating.class);
		System.out.println(rating);
		movieRatingList = new ArrayList<MovieRating>();
		movieRatingList.add(rating);
		// get Movie info from MovieInfo Service
		info = template.getForObject(infoUrl + movieId, MovieInfo.class);
		System.out.println(info);
		movieInfoList = new ArrayList<MovieInfo>();
		movieInfoList.add(info);
		// create userMovieDetails class object
		movieDetails = new UserMovieDetails();
		movieDetails.setUserId(userId);
		movieDetails.setMovieRatingList(movieRatingList);
		movieDetails.setMovieInfoList(movieInfoList);
		return movieDetails;
	}
	
	
}
