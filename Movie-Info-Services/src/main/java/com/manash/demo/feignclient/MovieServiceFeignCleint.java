package com.manash.demo.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.manash.demo.model.MovieInfo;

@FeignClient(value = "movie-info-service")
public interface MovieServiceFeignCleint {
	
	@RequestMapping("/info//{movieId}")
	public MovieInfo getMovie(@PathVariable("movieId") String movieId);

}
