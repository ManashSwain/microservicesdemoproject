package com.manash.demo.model;

import lombok.Data;

@Data
public class MovieInfo {
	
	private String movieId;
	private String movieName;
	private String desc;
	
	
	public MovieInfo(String movieId, String movieName, String desc) {
		super();
		this.movieId = movieId;
		this.movieName = movieName;
		this.desc = desc;
	}
	
	
	

}
