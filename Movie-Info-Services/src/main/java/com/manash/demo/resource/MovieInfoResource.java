package com.manash.demo.resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.manash.demo.model.MovieInfo;

@RestController
@RequestMapping("/info")
public class MovieInfoResource {
	
	@RequestMapping("/{movieId}")
	public MovieInfo getMovie(@PathVariable("movieId") String movieId) {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new MovieInfo(movieId,"WAR","Action");
	}

}
