package com.manash.demo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.manash.demo.model.MovieRating;
import com.manash.demo.service.RatingService;

@RestController
@RequestMapping(value = "/rating")
public class MovieRatingResource {
	
	@Autowired
	private RatingService service;
	
	@RequestMapping(value = "/{movieId}",method = RequestMethod.GET,produces = {"application/json"})
	public ResponseEntity<MovieRating> getRatings(@PathVariable("movieId") String movieId) {
		MovieRating rating=null;
		rating = service.getRating(movieId);
		
		return new ResponseEntity<MovieRating>(rating,HttpStatus.OK);
	}
	
	@RequestMapping(value = "/set",method = RequestMethod.POST,
			        consumes = {"application/json","application/xml"},
			        produces = {"application/json","application/xml"})
	public ResponseEntity<String> setUserRating(@RequestBody MovieRating rating){
		ResponseEntity<String> entity=null;;
		if(rating!=null) {
		      String msg=service.setRating(rating);
		      entity=new ResponseEntity<String>(msg, HttpStatus.OK);
		      return entity;
		}
		else {
			String msg="Please Check the Movie ID";
			entity=new ResponseEntity<String>(msg,HttpStatus.BAD_REQUEST);
			return entity;
		}
		
	}

}
