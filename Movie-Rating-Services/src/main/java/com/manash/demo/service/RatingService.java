package com.manash.demo.service;

import com.manash.demo.model.MovieRating;

public interface RatingService {
	
	public MovieRating getRating(String movieId);
	public String setRating(MovieRating ratings);

}
