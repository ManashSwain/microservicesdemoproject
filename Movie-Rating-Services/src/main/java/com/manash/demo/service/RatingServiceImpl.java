package com.manash.demo.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.manash.demo.model.MovieRating;

@Service("ratingService")
public class RatingServiceImpl implements RatingService {
	
	private static final Map<String,MovieRating> movieRatingMap=new HashMap<String,MovieRating>();

	@Override
	public MovieRating getRating(String movieId) {
		return movieRatingMap.get(movieId);
	}

	@Override
	public String  setRating(MovieRating ratings) {
		if(!movieRatingMap.containsKey(ratings.getMovieId())) {
			//add rating into the map collection
			movieRatingMap.put(ratings.getMovieId(),ratings);
			return "Thanks for your Rating!!!!!";
		}
		else {
			movieRatingMap.replace(ratings.getMovieId(),ratings);
			return "Your Rating Updated Successfully";
		}
		
	}
	
	
	

}
