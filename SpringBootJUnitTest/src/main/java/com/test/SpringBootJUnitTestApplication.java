package com.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.test.model.Employee;
import com.test.service.EmployeeService;

@SpringBootApplication
public class SpringBootJUnitTestApplication {
	
	@Autowired
	private EmployeeService service;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJUnitTestApplication.class, args);
		SpringBootJUnitTestApplication app=new SpringBootJUnitTestApplication();
		Employee emp1=new Employee(1,"a");
		Employee emp2=new Employee(2,"b");
		Employee emp3=new Employee(3,"c");
		System.out.println(app.service.insertEmployee(emp1));
		//System.out.println(app.service.getAll());
	}
	

}
