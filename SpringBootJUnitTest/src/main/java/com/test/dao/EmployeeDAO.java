package com.test.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.test.model.Employee;

@Repository
public class EmployeeDAO {

	private static Map<Integer, Employee> map = new HashMap<Integer, Employee>();

	public String saveEmployee(Employee emp) {
		if (map.get(emp.getEmpno()) == null) {
			map.put(emp.getEmpno(), emp);
			return "Employee Saved!!";
		} else {
			return "Duplicate";
		}

	}
	public Employee getEmpById(Integer empno) {
		return map.get(empno);
	}
	
	public List<Employee> getAllEmp(){
		List<Employee> empList=new ArrayList<Employee>();
		map.forEach((key,value)->{
			empList.add(value);
		});
		return empList;
	}
}
