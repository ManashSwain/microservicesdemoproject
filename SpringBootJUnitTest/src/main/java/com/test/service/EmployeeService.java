package com.test.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.dao.EmployeeDAO;
import com.test.model.Employee;

@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDAO dao;
	
	public String insertEmployee(Employee emp) {
		return dao.saveEmployee(emp);
	}
	
	public Employee getEmpById(Integer empno) {
		return dao.getEmpById(empno);
	}
	
	public List<Employee> getAll(){
		return dao.getAllEmp();
	}
	
	

}
